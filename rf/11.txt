## 11. Spiritual life is a Pilgrimage. 

The spiritual life is a pilgrimage which is joined by dif­ferent
persons, coming from different directions, along different routes,
passing through different terrains, using different modes of
sensibilities, and bringing with them for an offering their different
life-experiences. They sing and praise Him in different idioms, using
different images, metaphors and symbols. The wise say: Do not fight
about the symbols and images. Look behind at the heart which is one.

