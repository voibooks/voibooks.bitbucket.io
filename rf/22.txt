## 22. The Truth is Ready. 

Thus the only Book contains the only word of the only God, brought from
the seventh heaven by the only Son or the Last Messenger of God. You can
look it up now for any guidance though, according to the more orthodox
school, the best thing for you would be to get it interpreted by the
scholars and official dignitaries of your church. There are other
differences of this kind, and even sharp ones, but they are all agreed
that everything is ready now—you are ready for the truth and the truth
is ready for you. However to make the truth still more handy, the
theologians have reduced it *to* formulas, codified them and numbered
them for the convenience of their flock. The truths are pre-cooked, and
even pre-digested. They are packed and are then ready for export to the
pagans and the infidels.

Hinduism deals in no such truths. At any rate, it considers such truths
meaningless. Its truths are truths of a man’s own deeper being. **He has
to grow into them. The more he understands himself, the more he
understands these truths.** These cannot be put into formulas, or made
into numbered articles of faith. They cannot be easily handed down. They
are not meant for memorization, for repetition, for confession. The
truths the Hindu scriptures care for are received in a different way—
**through some kind of self-preparation, through self-churning. One has
to get ready for them.** One has to be “reborn” in order to receive
them. They are meant for the “twice-born”.

One can see for oneself that in this kind of approach, there is no place
for ‘’conversion”. True religion can neither be bor­rowed nor lent. A
man carries his religion within himself wherever he goes and he has to
be his own lamp. The conver­sion one experiences at this level is inner
and it is very different from the conversion that different countries
have known under the aegis of Christianity and Islam. These conversions
were political and military acts, or acts of aggressive hard sale.

Gnostic religions, more psychological in approach, realize that deep
truths of the spirit cannot adequately be imparted from outside however
hard one may try One may place a most exalted truth before a person, but
he will draw his own meanings out of it, meanings appropriate to his
particular level of purity and understanding. In fact, he would use
these truths for rationalizing and promoting his own petty ends.

But it does not mean that spiritual work is unimportant and that
spiritual teachers have no role. It only points to the difficulties of
the work. Knowing the difficulties, **Hinduism, therefore, does not try
to pronounce final truths *ex cathedra* on the other hand, it offers a
spiritual culture, a spiritual discipline to raise the capacity of a
seeker of truth.** It does not burden a man with truths and morals
beyond his capacity; on the other hand, it discusses many kinds of
*dharmas*, *atma-dharma* being the highest. **It helps different men
having different capacities and different starting-points to choose
their own *dharma.***

Thus we find that **Hinduism does not teach a God who reveals Himself
only to a chosen individual, letting others receive their truth from
this exclusive source.** On the other hand, it speaks of truths which
are revealed wherever there is seeking, the purity of aspiration, and
necessary preparation. Nor does it believe that there is only one
revealed book; in its view, the words of all seers, all *atma-darshins,*
past, present, and future, have scriptural value. It knows of course
that **not all visions are God-visions and not all excited utterances
made with foaming lips are words of God, that many revelations claiming
a divine source are in fact projections of a less laudable source. *Id*
sires many revelations, often a projection of the puerile and the
pernicious.** Yoga teaches us to be wary of them. But this is another
question and we shall not go into it here.

