## 27. Difference in behaviour towards Others. 

The overriding role of faith and belief has made Christianity and Islam
authoritarian and fundamentalist. On the contrary, a religion like
Hinduism in which reason, philosophy and psychology play a greater part
is more psycho-experimental in approach and freer in entertaining
alternative views. People could freely be monotheists, polytheists,
atheists, advaitins. **Hinduism speaks of truths which are capable of
verification by individuals** though the conditions of verification are
difficult to fulfil by their very nature. It is true that Hinduism gives
a high place to *shabda-pramana*, to the testimony of those who are in a
position to testify; **but its truths, in the last analysis, are a
matter of experience, capable of being tested by the individual,**
*ehipassika* (come and see), as Lord Buddha called them. Conse­quently,
Hinduism has been more concerned with explaining, examining,
experiencing, with *sadhana*, praxis, yoga. **Christianity, on the
contrary, has been concerned with dogmatically affirming and with
‘apologetically’ proving and justifying. Islam in its turn has refused
even to argue. It cuts short the argument by cutting off your head.**
Discussion, in its view, is merely waste of breath; it has found that
the logic of force is more convincing.

At the back of this sectarian thinking, we believe, is a very partial
and inadequate conception of the Godhead. **The Semitic approach to God
is too numerical and arithmetical, and not sufficiently spiritual**. It
has made a **fetish** of numbers but not looked at the heart of the
mystery. *Advaita*, *Ekam Sat*, **‘One Reality’ of the Hindus taught
them to see divinity also in Gods of other people; but the ‘One God’ of
Semitic religions taught them to see evil in Gods other than their own.
The destruc­tion of the temples and places of worship of other people is
an inevitable result of this approach.**

**Hindus had their wars but those were not religious ones. In the wake
of victory, the first thing a victorious Hindu king did was to offer
worship to the Gods of the conquered. It was enjoined by the laws and it
was also a common practice.** Manu taught that a victor should “duly
worship the Gods of the defeated people... honour their
righteousBrahmins... place on the throne a scion or relative of the
vanquished ruler... make authoritative the lawful customs of the
inhabitants, and honour their new king and his chief servants with
precious gifts.” (Manu. 7-201–203) But the Semitic religions embody a
very different psyche. **With them came the concept of religious wars, a
fanatic intolerance of the Gods and priests of other people, genocide,
vandalistic destruction, and colonial exploitation of the conquered
people.**

