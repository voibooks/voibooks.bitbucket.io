## 20. Gods to choose an only Favourite or Broker. 

On this question, the thinking of Semitic religions is very inadequate.
They believe that God chooses some favourite indivi­dual, his son or
prophet for his self-revelation, and from that point onwards, **others
must accept their truth second-hand.** Accor­ding to Hinduism, there is
no such arbitrariness. God-conscious- ness is an inherent quality of the
soul and one enters God- consciousness through sincere seeking. The
imported or smuggled intermediary is a gratuitous assumption.

