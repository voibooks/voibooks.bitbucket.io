## 12. The basis of Hindu Psyche– a Deeper Truth. 

The Hindu psyche has been shaped by this kind of religious teaching and
intuition. This approach has strength­ened the roots of religious
tolerance amongst the Hindus. Religious tolerance and religious freedom
are great liberal and humanist truths which have their roots in the
still deeper truths of the Spirit and the Self—truths which lie beyond
man’s ordinary reason, and also beyond his ordinary religiosity; even
beyond certain especial beliefs by which certain religions lay great
store, or certain strong emotions which they inculcate, or certain
obsessive roles which they assign to themselves.


