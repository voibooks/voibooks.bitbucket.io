## 26. Exclusive Brotherhood. 

The emphasis on a privileged revelation leading to the concept of a
privileged brotherhood or of a chosen church has affected Islam and
Christianity in more than one way. **It made their morals tribal and
their social organization power-oriented. Their organization is
eminently suited for outward, political expansion,** though a religious
dimension of a sort is not alto­gether lacking; a dimension which has
the power to bring emotional and even spiritual satisfaction to those
who are so minded. But the political thrust of their social organization
has been clear throughout history. **The Muslim *kalma* is more of a
battle-cry than a statement of a spiritual truth.** *Allah-u-Akbar* has
been heard as often in the battle-fields as in the mosques.

These religions grew with political power and **their religious
functionaries were more than priests. They were officials of the
state,** and even when they followed a separate discipline, they were
part of the same power establishment. **These religions represented
great theocracies.** The common men were governed by mullahs and
priests. Church functionaries administered laws claiming a divine
origin. They took care of the morals of the people and interpreted the
will of God for them. Even today in Europe where traditional religion
has ceased to be a serious thing, the vestiges of the old tie-up between
the state and the church still continue. In England, the priests of the
Anglican Church, the national religion, are paid employees of the state.
In Germany, the revenue for the country’s religious denomina­tions is
collected along with taxes by the state and made over to them.

**In Hinduism, there was no such analogous phenomenon. There was a
highly respected Brahmin class but its functions were priestly and
scholarly.** They taught; they advised; they presided on such occasions
as birth, wedding and funeral. But the deeper ethos of the Hindus was
shaped by monks and religious mendicants and even by such householders
who had, rightly or wrongly, the reputation of having ‘seen’ God. **But
Hinduism has known no Pope, no Khalifa, no *ex-Cathedra* pronouncements,
no fatwas,** no official credo or list of beliefs formulated by official
theologians meeting under the aegis of kings and feudal war-lords.

