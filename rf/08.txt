## 8. God is not a symbol of one’s Ego. 

God has made man in His image; but man too makes God in his image. He
makes a little god out of his ambitions, hatreds, sensuality and
interests; then he sets him up on a pedestal and worships him; and he
also forces others in worshipping him. And this he says is for the
greater glory of god and for magnifying him. But this is really for his
own glory and for his own puffiness. To claim exclusive knowledge of God
is pretentious and egoistical. It is spiritual aggression leading to the
next inevitable step of physical aggrandizement.

